export default {
    methods: {
        closeLoginModal() {
            this.$store.commit('TOGGLE_LOGIN_MODAL',false)
        },
        showLoginModal() {
            this.$store.commit('TOGGLE_LOGIN_MODAL',true)
        },
        closeNotifyModal() {
          this.$store.commit('TOGGLE_NOTIFY_MODAL',false)
        },
        showNotifyModal() {
          this.$store.commit('TOGGLE_NOTIFY_MODAL',true)
        },
        showLocationModal() {
          this.$store.commit('TOGGLE_LOCATION_MODAL',true)
        },
        closeLocationModal() {
          this.$store.commit('TOGGLE_LOCATION_MODAL',false)
        }
    },
}
