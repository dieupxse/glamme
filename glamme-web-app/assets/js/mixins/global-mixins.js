// jshint ignore: start
import {mapGetters} from 'vuex'
export default {
  notifications: {
    showNotification: { // You can have any name you want instead of 'showLoginError'
      title: '',
      message: '',
      type: '', // You also can use 'VueNotifications.types.error' instead of 'error'
      timeout: 3000
    }
  },
    computed: {
        ...mapGetters({
            seoHome: 'GET_SEO_HOMEPAGE',
            $mobile: 'GET_MOBILE',
            tran: 'GET_TRANSLATE',
            $lang: 'GET_CURRENT_LANG',
            $location: 'GET_CURRENT_LOCATION'
        })
    }
}
