import {http} from './helper'
import {apiUrl} from '@/assets/js/commons/constants'

export default {
  getMenuItemsByMenuId (id,lang) {
    var params= {
      orderBy: 'order',
      order: 'asc'
    }
    return new Promise((resolve, reject) => {
      http.get(`${apiUrl.MENU}${id}?language=${lang}`, {params: params})
        .then(response => {
          resolve(response.data)
        }).catch(err => {
          reject(err.status)
        })
    })
  },
  getMenuItems () {
    return menuItems
  }
}
