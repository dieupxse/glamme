import {POST_PER_PAGE} from '../commons/constants'
import {callApi} from './helper'

export default {
  applyDiscountCode (token, info) {
    return new Promise((resolve, reject) => {
      callApi(token).post(`/code/api/DiscountCode/UseCode?Discode=${info.code}&productId=${info.productId}`).then(response => {
        resolve(response.data)
      }).catch(err => {
        reject(err)
      })
    })
  }
}
