import {POST_PER_PAGE} from '../commons/constants'
import {axios} from './helper'

export default {
  getListArtist (isHome) {
    return new Promise((resolve, reject) => {
      axios.get(`goods/api/Artist?ishome=${isHome}`)
        .then(response => {
          resolve(response.data)
        }).catch(err => {
          reject(err)
        })
    })
  },
  registerArtist (data) {
    return new Promise((resolve, reject) => {
      // axios.post(`http://localhost:49925/api/artist/register`, data)
      axios.post(`goods/api/artist/register`, data)
        .then(response => {
          resolve(response.data)
        }).catch(err => {
          reject(err)
        })
    })
  },
  getAllProvince () {
    return new Promise((resolve, reject) => {
      axios.get(`ultil/api/ulitity/GetAllProvince`)
        .then(response => {
          resolve(response.data)
        }).catch(err => {
          reject(err)
        })
    })
  },
  getDistrictsByProvinceId (id) {
    return new Promise((resolve, reject) => {
      axios.get(`ultil/api/ulitity/GetDistrictsByProvinceId/${id}`)
        .then(response => {
          resolve(response.data)
        }).catch(err => {
          reject(err)
        })
    })
  }
}
