import {callApi} from './helper'

export default {
  getProvince(token, keyword="", page=1, rowPerPage=20, orderby="CreateDate", order="desc") {
    return new Promise((resolve, reject) => {
      callApi(token).get(`/config/api/province`, {
            params: {
                keyword,
                page,
                rowPerPage,
                orderby,
                order
            }
        }).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
    })
  },
  getProvinceBySlug(token, slug) {
    return new Promise((resolve, reject) => {
      callApi(token).get(`/config/api/province/slug/${slug}`).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
    })
  },
  getProvinceById(token, id) {
    return new Promise((resolve, reject) => {
      callApi(token).get(`/config/api/province/${id}`).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
    })
  },
  createProvince(token, data) {
    return new Promise((resolve, reject) => {
        http(token).post(`/config/api/province`, data).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
      })
  },
  updateProvince(token,id,data) {
    return new Promise((resolve, reject) => {
        http(token).put(`/config/api/province/${id}`, data).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
      })
  },
  deleteProvince(token,id) {
    return new Promise((resolve, reject) => {
        http(token).delete(`/config/api/province/${id}`).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
      })
  }
}
