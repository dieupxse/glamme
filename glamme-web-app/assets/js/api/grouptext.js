import {callApi} from './helper'
export default {
  getGroupText(token, keyword="",language="vi", page=1, rowPerPage=20, orderby="CreateDate", order="desc") {
    return new Promise((resolve, reject) => {
        callApi(token).get(`/config/api/grouptext`, {
            params: {
                keyword,
                language,
                page,
                rowPerPage,
                orderby,
                order
            }
        }).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
    })
  },
  getGroupTextBySlug(token, slug,language="vi") {
    return new Promise((resolve, reject) => {
        callApi(token).get(`/config/api/grouptext/slug/${slug}?language=${language}`).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
    })
  },
  getGroupTextByGroupId(token, id) {
    return new Promise((resolve, reject) => {
        callApi(token).get(`/config/api/grouptext/${id}`).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
    })
  },
  createGroupText(token, data) {
    return new Promise((resolve, reject) => {
        callApi(token).post(`/config/api/grouptext`, data).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
      })
  },
  updateGroupText(token,id,data) {
    return new Promise((resolve, reject) => {
        callApi(token).put(`/config/api/grouptext/${id}`, data).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
      })
  },
  deleteGroupText(token,id) {
    return new Promise((resolve, reject) => {
        callApi(token).delete(`/config/api/grouptext/${id}`).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
      })
  }
}
