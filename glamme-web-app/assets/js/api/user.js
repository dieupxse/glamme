import {axios} from './helper'

export default {
  postSubscribeEmail (data) {
    return new Promise((resolve, reject) => {
      axios.post(`/user/api/Subscrible`, data).then(response => {
        resolve(response)
      }).catch(err => {
        reject(err)
      })
    })
  }
}