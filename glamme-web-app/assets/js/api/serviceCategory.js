import {POST_PER_PAGE} from '../commons/constants'
import {axios} from './helper'

export default {
  getCurrentServiceCategory (slug) {
    return new Promise((resolve, reject) => {
      axios.get(`product/api/Category/GetCategoryBySlug/${slug}`)
        .then(response => {
          resolve(response.data)
        }).catch(err => {
          reject(err)
        })
    })
  },
  getListServiceCategory (artistId, isShowHome, page, orderby, order) {
    if(isShowHome === undefined) isShowHome = false
    return new Promise((resolve, reject) => {
      axios.get(`/product/api/category?&artistId=${artistId}&isShowHome=${isShowHome}&page=${page}&rowPerPage=${POST_PER_PAGE}&orderby=${orderby}&order=${order}`)
        .then(response => {
          resolve(response.data)
        }).catch(err => {
          reject(err)
        })
    })
  }
}
