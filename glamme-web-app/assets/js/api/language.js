import {callApi} from './helper'
// import axios from 'axios'

// const http1 = (token) =>  axios.create({
//   headers: {
//     Authorization: `Bearer ${token}`,
//     "content-type": "application/json",
//   },
//   baseURL: '',
// })
export default {
  getLanguage( keyword="", page=1, rowPerPage=20, orderby="CreateDate", order="desc") {
    return new Promise((resolve, reject) => {
      callApi('').get(`/config/api/language`, {
            params: {
                keyword,
                page,
                rowPerPage,
                orderby,
                order
            }
        }).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
    })
  },
}
