import {axios} from './helper'
export default {
  getMostUsedTag (take) {
    return new Promise((resolve, reject) => {
      axios.get(`/cms/api/tag/GetMostUsedTags?take=${take}`)
        .then(response => {
          resolve(response.data)
        }).catch(err => {
          reject(err.status)
        })
    })
  }
}