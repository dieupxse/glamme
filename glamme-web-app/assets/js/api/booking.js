import {http, axios, callApi} from './helper'
export default {
  booking (bookingInfo) {
    return new Promise((resolve, reject) => {
      axios.post(`/booking/api/booking`, bookingInfo).then(response => {
        resolve(response)
      }).catch(err => {
        reject(err)
      })
    })
  },
  GetBookingByCurrentACcount(token, page, rowPerPage, statusType) {
    return new Promise((resolve, reject) => {
        callApi(token).get(`/booking/api/booking/GetByCurrentAccount?statusType=${statusType}&page=${page}&rowPerPage=${rowPerPage}`)
        .then(response => {
          resolve(response.data)
        }).catch(err => {
          reject(err)
        })
    })
  },
  UpdateBooking(token, id, status){
    return new Promise((resolve, reject) => {
      callApi(token).put(`/booking/api/booking/UpdateStatus/${id}?status=${status}`).then(response => {
        resolve(response)
      }).catch(err => {
        reject(err)
      })
    })
  }
}
