const bannerItems = [
  {
    id: 1,
    src: 'http://congnghe247.info/wp-content/uploads/2018/03/image_1.jpg',
    link: '#',
    target: ''
  },
  {
    id: 2,
    src: 'http://congnghe247.info/wp-content/uploads/2018/03/image_2.jpg',
    link: '#',
    target: ''
  },
  {
    id: 3,
    src: 'http://congnghe247.info/wp-content/uploads/2018/03/image_3.jpg',
    link: '#',
    target: '_blank'
  }
]

export default {
  getBannerItems () {
    return bannerItems
  }
}
