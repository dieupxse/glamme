import {callApi} from './helper'
export default {
  loginFacebook(token, data) {
    return new Promise((resolve, reject) => {
        callApi(token).post(`/account/api/account/AuthenByFacebook`, data).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
    })
  },
  login(token, data) {
    return new Promise((resolve, reject) => {
        callApi(token).post(`/account/api/account/AuthenByEmailPass`, data).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
    })
  },
  register(token, data) {
    return new Promise((resolve, reject) => {
        callApi(token).post(`/account/api/account/register`, data).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
    })
  },
  getCurrentUser (token) {
    return new Promise((resolve, reject) => {
        callApi(token).get(`/account/api/account/GetCurrentAccount`).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
    })
  },
  updateCurrentUser(token, auth) {
    return new Promise((resolve, reject) => {
      callApi(token).put(`/account/api/account`, auth).then(response => {
        resolve(response)
      }).catch(err => {
        reject(err)
      })
    })
  }
}
