
import {POST_PER_PAGE} from '@/assets/js/commons/constants'
import {axios} from './helper'
export default {
  getPostsByListCatId (param) {
    // var result = []
    var page = 1
    var rowPerPage = 4
    var orderby = 'createdDate'
    var order = 'desc'
    var listCats = param.join(',')
    return new Promise((resolve, reject) => {
      axios.get(`/cms/api/post/GetPostsByListCategory?listCatIds=${listCats}&page=${page}&rowPerPage=${rowPerPage}&order=${order}&orderby=${orderby}`)
        .then(response => {
          resolve(response.data)
        }).catch(err => {
          reject(err.status)
        })
    })
  },
  getPost (postId) {
    return new Promise((resolve) => {
      axios.get(`/cms/api/post/${postId}`)
        .then(response => {
          resolve(response.data)
        })
    })
  },
  getPostBySlug (slug) {
    return new Promise((resolve, reject) => {
      axios.get(`/cms/api/post/slug/${slug}`)
        .then(response => {
          resolve(response.data)
        }).catch((err) => {
          reject(err)
        })
    })
  },
  getListPorstsByCateSlug (cateSlug, status, page, rowPerPage = 10) {
    return new Promise((resolve, reject) => {
      axios.get(`/cms/api/post/GetPostByCatSlug?catslug=${cateSlug}&status=${status}&page=${page}&rowPerPage=${rowPerPage}`)
        .then(response => {
          resolve(response.data)
        }).catch((err) => {
          reject(err)
        })
    })
  },
  getListPostsByTagSlug (tagSlug, status, page, rowPerPage = 10) {
    return new Promise((resolve, reject) => {
      axios.get(`/cms/api/post/GetPostByTagSlug?tag=${tagSlug}&status=${status}&page=${page}&rowPerPage=${rowPerPage}`)
        .then(response => {
          resolve(response.data)
        }).catch((err) => {
          reject(err)
        })
    })
  },
  getListNewsPosts (status, page, rowPerPage = 10, orderby ,order) {
    return new Promise((resolve, reject) => {
      axios.get(`/cms/api/post/GetListPostForCMS?&status=${status}&page=${page}&rowPerPage=${rowPerPage}&orderby=${orderby}&order=${order}`)
        .then(response => {
          resolve(response.data)
        }).catch((err) => {
          reject(err)
        })
    })
  },
  getListMostViewPosts (status,take) {
    return new Promise((resolve, reject) => {
      axios.get(`/cms/api/post/getmostvew?status=${status}&take=${take}`)
        .then(response => {
          resolve(response.data)
        }).catch((err) => {
          reject(err)
        })
    })
  },
  getPosts (params) {
    return new Promise((resolve, reject) => {
      axios.get(`cms/api/post`,{
        params: params
      })
        .then(response => {
          resolve(response.data)
        }).catch((err) => {
          reject(err)
        })
    })
  }
}
