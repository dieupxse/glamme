import {POST_PER_PAGE} from '../commons/constants'
import {axios} from './helper'

export default {
  getProductByCategoryId (cateId, page, orderby, order, isshowhome, location,language='vi') {
    // if (cateSlug === undefined) {
    //   cateSlug = ''
    // }
    return new Promise((resolve, reject) => {
      axios.get(`https://api.glamme.vn/goods/api/Product?categoryId=${cateId}&page=${page}&rowPerPage=${POST_PER_PAGE}&orderby=${orderby}&order=${order}&isshowhome=${isshowhome}&location=${location}&language=${language}`)
        .then(response => {
          resolve(response.data)
        }).catch(err => {
          console.log(err)
        })
    })
  },
  getListImagesDiscover (page, rowPerPage, isshowhome,location) {
    return new Promise((resolve, reject) => {
      axios.get(`https://api.glamme.vn/goods/api/image?page=${page}&rowPerPage=${rowPerPage}&isShowHome=${isshowhome}&location=${location}`)
      .then(response => {
        resolve(response.data)
      }).catch(err => {
        console.log(err)
      })
    })
  },
  getListImagesDiscoverByTag (page, rowPerPage, Slug, location) {
    return new Promise((resolve, reject) => {
      axios.get(`https://api.glamme.vn/goods/api/image/GetListImagesByTag?page=${page}&rowPerPage=${rowPerPage}&tagSlug=${Slug}&location=${location}`)
      .then(response => {
        resolve(response.data)
      }).catch(err => {
        console.log(err)
      })
    })
  },
  getListTagDiscover (status) {
    return new Promise((resolve, reject) => {
      axios.get(`https://api.glamme.vn/goods/api/tag?isShowOnPage=${status}`)
      .then(response => {
        resolve(response.data)
      }).catch(err => {
        console.log(err)
      })
    })
  },
  sendEmail (email) {
    return new Promise((resolve, reject) => {
      axios.post(`/ultil/api/email`, email).then(response => {
        resolve(response)
      }).catch(err => {
        reject(err)
      })
    })
  }
}
