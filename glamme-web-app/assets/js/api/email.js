import {POST_PER_PAGE} from '../commons/constants'
import {http, axios} from './helper'

export default {
  getTemplateContentEmail (groupId) {
    return new Promise((resolve, reject) => {
      axios.get(`/config/api/grouptext/${groupId}`)
        .then(response => {
          resolve(response.data)
        }).catch(err => {
          console.log(err)
        })
    })
  }
}
