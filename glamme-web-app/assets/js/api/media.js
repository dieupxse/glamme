import {callApi} from './helper'
export default {
  uploadPhoto (token, info) {
    return new Promise((resolve, reject) => {
      callApi(token).post(`/media/api/photo`, info).then(response => {
        resolve(response.data)
      }).catch(err => {
        reject(err)
      })
    })
  }
}
