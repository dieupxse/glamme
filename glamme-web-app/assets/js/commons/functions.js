import moment from 'moment'
const genBlogCategoryPath = (path,lang) => {
  if(lang!=null && lang!=''&&lang!=undefined) {
    return `/${lang}/blog/${path}`
  } else {
    return `/blog/${path}`
  }

}
const genBlogPostPath = (path,lang) => {
  if(lang!=null && lang!=''&&lang!=undefined) {
    return `/${lang}/blog/${path}.html`
  }
    return `/blog/${path}.html`
}
const genBlogTagPath = (slug,lang) => {
  if(lang!=null && lang!=''&&lang!=undefined) {
    return `/${lang}/blog/tag/${slug}.html`
  }
    return `/blog/tag/${slug}.html`
}
const genBlogPagePath = (slug,lang) => {
  if(lang!=null && lang!=''&&lang!=undefined) {
    return `/${lang}/page/${slug}.html`
  }
  return `/page/${slug}.html`
}
const genLinkTagDiscover = (slug,location,lang) => {
  var langPrefix = ''
  var locationPrefix = ''
  if(lang!=null && lang!=''&&lang!=undefined) {

    langPrefix = `/${lang}`
  }
  if(location!=='' && location!=null&&location!=undefined) {
    locationPrefix = `/${location}`
  }
  return `${langPrefix}/get-inspired${locationPrefix}/${slug}.html`

}
const genLinkDiscoverDetail = (slug,lang) => {
  if(lang!=null && lang!=''&&lang!=undefined) {
    return `/${lang}/get-inspired/info/${slug}.html`
  }
  return `/get-inspired/info/${slug}.html`
}
const formatDate = (date,format) => {
    return moment(date).format(format);
}
const FormatThousand = (value, unit = "đ") => {
  return `${(value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.'))} ${unit}`
}
const getStatusLabel = (status) => {
  switch(status) {
      case 1:
          return "Đang chờ xác nhận"
      case 2:
          return "Đã xác nhận"
      case 3:
          return "Đã hoàn tất"
      case 4:
          return "Đã hủy"
      default:
          return ""
  }
}
const getLink = (slug, type,lang) => {
    switch(type) {
        case 'custom-link':
            return slug.indexOf('http') != -1 ? `${slug}` : `/${slug}`
        case 'category':
            return genBlogCategoryPath(slug,lang)
        case 'page':
            return genBlogPagePath(slug,lang)
        default:
            return '/'
    }
}
const stripHtml = (html) =>
{
   var tmp = document.createElement("DIV");
   tmp.innerHTML = html;
   return tmp.textContent || tmp.innerText || "";
}
export default {
    genBlogCategoryPath,
    genBlogPostPath,
    genBlogTagPath,
    genBlogPagePath,
    formatDate,
    FormatThousand,
    getLink,
    stripHtml,
    genLinkTagDiscover,
    genLinkDiscoverDetail,
    getStatusLabel
}
