const API_BASE_URL = 'https://api.glamme.vn'
const POST_PER_PAGE = 10
const POST_PER_PAGE_IN_SERVICE_CATEGORY = 9
const RECENT_VIEW_PRODUCT_NUMBER = 5
const LOCAL_STORAGE_CART_KEY = 'cart'
const LOCAL_STORAGE_RECENT_VIEW_PRODUCT_KEY = 'recent_view_product'
const SITE_URL = 'https://glamme.vn'
const TAG_NUMBER = 18
const listTemplateComponents = [
    'dia-diem',
    'uu-dai',
    'blog' // blog
]
const postStatus = {
  DRAF: 0,
  PUBLISH: 1,
  HIDE: 2
}
const isShowConstant = {
  ShowAll: -1,
  False: 0,
  True: 1
}
const callApi = function () {
    alert('call api')
}
const apiUrl = {
    MENU_DETAIL: `${API_BASE_URL}/config/api/MenuDetail`,
    MENU: `${API_BASE_URL}/config/api/menu/`
}
const gaConfig = {
    id: 'UA-XXXXXXXX-X'
}
const fbSdkConfig = {
    appId: '1904896796489976',
    cookie: true,
    xfbml: true,
    autoLogAppEvents : true,
    version: 'v3.2'
  }
const TAG_COLOR = ['#593E45','#F2A7BB','#F2A7DE','#A7F2DD','#F2A6B7','#FC8C7C','#A7F2DD']
const CLIENT_AUTH_TOKEN = "XD_TT_DM"
const CLIENT_AUTH_refreshToken = "XS_TT_DM"
const LOCATION_REDIRECT = ['/','/services','/get-inspired']
const FORCE_REDIRECT = [{src: '/kham-pha', des: '/get-inspired'}, {src: '/dich-vu', des: '/services'}, {src: '/huong-dan-dat-lich', des: '/how-it-works'}]
export {
    SITE_URL,
    API_BASE_URL,
    POST_PER_PAGE,
    POST_PER_PAGE_IN_SERVICE_CATEGORY,
    RECENT_VIEW_PRODUCT_NUMBER,
    LOCAL_STORAGE_CART_KEY,
    LOCAL_STORAGE_RECENT_VIEW_PRODUCT_KEY,
    callApi,
    listTemplateComponents,
    postStatus,
    apiUrl,
    fbSdkConfig,
    gaConfig,
    TAG_NUMBER,
    TAG_COLOR,
    CLIENT_AUTH_TOKEN,
    isShowConstant,
    CLIENT_AUTH_refreshToken,
    LOCATION_REDIRECT,
    FORCE_REDIRECT
}
