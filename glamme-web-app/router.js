import Vue from 'vue'
import Router from 'vue-router'

import Index from '@/pages/index'
//blog page
import BlogIndex from '@/pages/blog/index'
import BlogCategory from '@/pages/blog/category/category'
import BlogPost from '@/pages/blog/post/post'
import BlogTag from '@/pages/blog/tag/tag'
//Blog Page
import BlogPage from '@/pages/blog/page/page'
//service page
import ServiceIndex from '@/pages/dich-vu/index'
//booking
import BookStepOne from '@/pages/booking/bookingStepOne'
import BookStepTwo from '@/pages/booking/bookingStepTwo'
import BookStepThree from '@/pages/booking/bookingStepThree'

//import Booking from '@/pages/booking/index'
//landing page
import BeautyOnDemand from '@/pages/landing/beautyOnDemand'
import Discover from '@/pages/landing/discover'
import DiscoverModal from '@/components/pages/discover/discover-modal'
import ArtistRegister from '@/pages/landing/artistRegister'
import bookingHistoryPage from '@/pages/landing/bookingHistoryPage'
import infoLogin from '@/pages/landing/infoLogin'

Vue.use(Router)

export function createRouter() {
  return new Router({
    mode: 'history',
    routes: [
      {
        path: '/',
        component: Index
      },

      //blog route
      {
        path: '/blog',
        component: BlogIndex,
      },
      {
        path: '/blog/:postSlug.html',
        component: BlogPost
      },
      {
        path: '/blog/tag/:tagSlug.html',
        component: BlogTag
      },
      {
        path: '/blog/:categorySlug',
        component: BlogCategory
      },
      {
        path: '/services',
        component: ServiceIndex
      },
      {
        path: '/services/:location',
        component: ServiceIndex
      },
      {
        path: '/booking',
        component: BookStepOne
      },
      {
        path: '/booking/step-one',
        component: BookStepOne
      },
      {
        path: '/booking/step-two',
        component: BookStepTwo
      },
      {
        path: '/booking/step-three',
        component: BookStepThree
      },
      {
        path: '/booking/step-one/:location',
        component: BookStepOne
      },
      {
        path: '/booking/step-two/:location',
        component: BookStepTwo
      },
      {
        path: '/booking/step-three/:location',
        component: BookStepThree
      },
      {
        path: '/booking/:location',
        component: BookStepOne
      },
      {
        path: '/how-it-works',
        component: BeautyOnDemand
      },
      {
        path: '/get-inspired',
        component: Discover
      },
      {
        path: '/get-inspired/:location/:tagSlug.html',
        component: Discover
      },
      {
        path: '/get-inspired/:tagSlug.html',
        component: Discover
      },
      {
        path: '/get-inspired/:location',
        component: Discover
      },
      {
        path: '/page/:postSlug.html',
        component: BlogPage
      },
      {
        path: '/tro-thanh-artist',
        component: ArtistRegister
      },
      {
        path: '/lich-hen',
        component: bookingHistoryPage
      },
      {
        path: '/tai-khoan',
        component: infoLogin
      },
      //language
      {
        path: '/:lang/blog',
        component: BlogIndex,
      },
      {
        path: '/:lang/blog/:postSlug.html',
        component: BlogPost
      },
      {
        path: '/:lang/blog/tag/:tagSlug.html',
        component: BlogTag
      },
      {
        path: '/:lang/blog/:categorySlug',
        component: BlogCategory
      },
      {
        path: '/:lang/services',
        component: ServiceIndex
      },
      {
        path: '/:lang/services/:location',
        component: ServiceIndex
      },
      //booking
      {
        path: '/:lang/booking',
        component: BookStepOne
      },
      {
        path: '/:lang/booking/step-one',
        component: BookStepOne
      },
      {
        path: '/:lang/booking/step-two',
        component: BookStepTwo
      },
      {
        path: '/:lang/booking/step-three',
        component: BookStepThree
      },
      {
        path: '/:lang/booking/:location',
        component: BookStepOne
      },
      {
        path: '/:lang/booking/step-one/:location',
        component: BookStepOne
      },
      {
        path: '/:lang/booking/step-two/:location',
        component: BookStepTwo
      },
      {
        path: '/:lang/booking/step-three/:location',
        component: BookStepThree
      },
      {
        path: '/:lang/how-it-works',
        component: BeautyOnDemand
      },

      {
        path: '/:lang/get-inspired/:location/:tagSlug.html',
        component: Discover
      },
      {
        path: '/:lang/get-inspired/:tagSlug.html',
        component: Discover
      },
      {
        path: '/:lang/get-inspired/:location',
        component: Discover
      },
      {
        path: '/:lang/get-inspired',
        component: Discover
      },
      {
        path: '/:lang/page/:postSlug.html',
        component: BlogPage
      },
      {
        path: '/:lang/tro-thanh-artist',
        component: ArtistRegister
      },
      {
        path: '/:lang/lich-hen',
        component: bookingHistoryPage
      },
      {
        path: '/:lang/tai-khoan',
        component: infoLogin
      },
      {
        path: '/:lang/:location',
        component: Index
      },
      {
        path: '/:location',
        component: Index
      },
    ],
    scrollBehavior (to, from, savedPosition) {
      if (to.path.indexOf('/kham-pha') <= -1
          || from.path=='/'
          || from.path == '/blog'
          || from.path == '/dich-vu'
          || from.path == '/huong-dan-dat-lich'
          || from.path == '/lich-hen') {
        return { x: 0, y: 0 }
      }
    }
  })
}
