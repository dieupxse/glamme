import Vue from 'vue'
import VueBootstrapDatePicker from 'vue-bootstrap-datetimepicker'

Vue.use(VueBootstrapDatePicker)
