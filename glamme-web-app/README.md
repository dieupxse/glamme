# glamme-web-app

> Glamme web app

## Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```
## Docker guilde
``` bash
## GLAMME BUILD GUILD
# build for app

$ docker build -t glamme/app  .
$ docker run -d -p 3000:3000 --name glamme_app glamme/app
# if docker exist then delete
$ docker rm <containerid>

# access container
$ docker exec -it <container name> /bin/bash
```
For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).
