import {LOCATION_REDIRECT, FORCE_REDIRECT} from '@/assets/js/commons/constants'
export default function ({store, app, params,route, redirect}) {
  var path = route.path
  var location = app.location
  var lang = app.lang
  var isPathChange = false;
  var defaultLang = 'vi'
  var defaultLocation = 'ho-chi-minh'
  var isLocationExist = true
  var isLangExist = true
  if(path.indexOf('/vi/')>-1) {
    path = path.replace('/vi/','/');
    isPathChange = true
  }
  // console.log('path ===>',path)
  var force_red = FORCE_REDIRECT.filter(e=> path.indexOf(e.src) > -1);
  if(force_red.length>0) {
    // console.log('oldPath ==>',path)
    path = path.replace(force_red[0].src, force_red[0].des);
    // console.log('new path ==>',path)
    redirect(path)
    return;
  }
  var locations = store.state.location.locations
  if(locations!=null && locations!=undefined) {
    isLocationExist = locations.filter(e=>e.slug==location).length > 0;
  }
  var languages = store.state.language.languages
  if(languages!=null && languages!=undefined) {
    isLangExist = languages.filter(e=>e.slug==lang).length > 0;
  }

  if(!isLangExist || !isLocationExist) {
    lang = isLangExist ? lang : defaultLang;
    location = isLocationExist ? location : defaultLocation;
    redirect(`/${lang}/${location}`)
  }
  // console.log('location ==>',location)
  if(location&&(location.indexOf('favicon')>-1 || location.indexOf('bootstrap')>-1) ) return;
  if(location!==null && location!=='' && location!=undefined) {
    if(LOCATION_REDIRECT.indexOf(path)>-1) {
      // console.log('redirect ==>',`${path}/${location}`)

      if(path=='/') {
        redirect(`/${location}`)
      } else {
        redirect(`${path}/${location}`)
      }
    }
  } else {
    var location = app.$cookies.get('location')
    if(location!=null && location!=='' && location!=undefined) {
      if(LOCATION_REDIRECT.indexOf(path)>-1) {
        // console.log('redirect ==>',`${path}/${location}`)
        if(path=='/') {
          redirect(`/${location}`)
        } else {
          redirect(`${path}/${location}`)
        }

      }
    }
  }
  if(isPathChange) {
    redirect(path)
    isPathChange = false;
  }
  return;
}
