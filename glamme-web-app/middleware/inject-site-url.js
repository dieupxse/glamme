import {SITE_URL} from '@/assets/js/commons/constants'
import Translate from '@/assets/js/translation/language'
export default function (context) {
    const { req, store, params, app } = context
    if(req) {
        var MobileDetect = require('mobile-detect'),
        md = new MobileDetect(req.headers['user-agent']);
        var mobile = {
            isMobile: md.mobile() != null,
            isPhone : md.phone () !=null,
            isTablet: md.tablet() !=null,
            os: md.os()
        }
        store.commit('SET_MOBILE',mobile)
    }
    if(params.lang!=null && params.lang!=''&&params.lang!=undefined) {
      context.app.lang = params.lang
      store.commit('SET_CURRENT_LANG',params.lang)
    } else {
      context.app.lang = 'vi'
      store.commit('SET_CURRENT_LANG','vi')
    }
    if(params.location&&params.location.indexOf('favicon')==-1 && params.location.indexOf('bootstrap')==-1) {
      app.$cookies.set('location',params.location)
      store.commit('SET_CURRENT_LOCATION',params.location)
    }
    context.app.location = params.location ? params.location: app.$cookies.get('location')
    store.commit('SET_TRANSLATE',Translate[context.app.lang]);
    context.currentUrl = `${SITE_URL}${context.route.fullPath}`
}
