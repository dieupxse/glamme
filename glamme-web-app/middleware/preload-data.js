import menuApi from '@/assets/js/api/menu'
import groupTextApi from '@/assets/js/api/grouptext'
import {SITE_URL} from '@/assets/js/commons/constants'
export default function (context) {
    const {route, store, params, app} = context
    var lang = app.lang
    return Promise.all([menuApi.getMenuItemsByMenuId(2,lang),
                        groupTextApi.getGroupTextBySlug('','seo-homepage',lang),
                        menuApi.getMenuItemsByMenuId(1,lang)
                      ])
                        .then(values=> {
      var blogMenu = values[0];
      store.commit("setBlogMenuItems",blogMenu.menuDetails)
      var headerMenu = values[2];
      store.commit("SET_HEADER_MENU", headerMenu)
      var seoHome = values[1].data.groupText
      var striptags = require('striptags')
      store.commit("SET_SEO_HOMEPAGE", {
        seoTitle: seoHome.title,
        seoKeyword: seoHome.subTitle,
        seoDescription: striptags(seoHome.description),
        seoUrl: `${SITE_URL}${route.fullPath}`
      })

    })
}
