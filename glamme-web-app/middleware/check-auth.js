import {CLIENT_AUTH_TOKEN, CLIENT_AUTH_refreshToken,POST_PER_PAGE} from '@/assets/js/commons/constants'
import authApi from '@/assets/js/api/client-authen'
import BookingApi from '@/assets/js/api/booking'

export default function ({store, route, redirect, app}) {
    var authToken = app.$cookies.get(CLIENT_AUTH_TOKEN)
    var RefreshAuthToken = app.$cookies.get(CLIENT_AUTH_refreshToken)
    var dateTime = new Date()
    var setDateExpire = (date) => {
      dateTime.setDate(dateTime.getDate() + date)
      return dateTime
    }
    var statusType = store.getters.GET_BOOKING_HISTORY_TYPE
    if(authToken) {
        return authApi.getCurrentUser(authToken).then((rs)=> {
            store.commit('SET_AUTH',rs.data)
            return BookingApi.GetBookingByCurrentACcount(authToken,1, POST_PER_PAGE, statusType).then(listBK => {
              store.commit('SET_LISTBOOKINGHISTORY',listBK.data)
              if(listBK.meta.totalPage <= 1){
                store.commit('SET_BOOKING_LOADMORE', false)
              }
              else{
                store.commit('SET_BOOKING_LOADMORE', true)
              }
            })
        }).catch((err)=> {
            app.$cookies.remove(CLIENT_AUTH_TOKEN)
            store.commit('REMOVE_AUTH')
        })
    }
    else{
      if(RefreshAuthToken){
        return authApi.login('', {
          refreshToken: RefreshAuthToken,
          grantType: "refreshToken"
        }).then(rs => {
          app.$cookies.remove(CLIENT_AUTH_TOKEN)
          app.$cookies.remove(CLIENT_AUTH_refreshToken)
          app.$cookies.set(CLIENT_AUTH_TOKEN,rs.data.token, {expires: setDateExpire(rs.data.expTokenDate)})
          app.$cookies.set(CLIENT_AUTH_refreshToken,rs.data.refreshToken, {expires: setDateExpire(rs.data.expRefreshTokenDate)})

          return authApi.getCurrentUser(rs.data.token).then((us)=> {
            store.commit('SET_AUTH',us.data)
            return BookingApi.GetBookingByCurrentACcount(rs.data.token,1, POST_PER_PAGE, statusType).then(listBK => {
              store.commit('SET_LISTBOOKINGHISTORY',listBK.data)
              if(listBK.meta.totalPage <= 1){
                store.commit('SET_BOOKING_LOADMORE', false)
              }
              else{
                store.commit('SET_BOOKING_LOADMORE', true)
              }
            })
          }).catch((err)=> {
              console.log(err.response)
          })
        }).catch((err)=> {
            app.$cookies.remove(CLIENT_AUTH_refreshToken)
            store.commit('REMOVE_AUTH')
        })
      }
    }
}
