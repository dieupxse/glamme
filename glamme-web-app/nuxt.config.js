const webpack = require('webpack')
const axios = require('axios')
module.exports = {
  debug: false,
  /*
  ** Headers of the page
  */
  build: {
    vendor: [
      'axios',
      'jquery',
      'moment',
      'vue-lazyload',
      'striptags'
    ],
    plugins: [
      new webpack.ProvidePlugin({
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
        $: 'jquery',
        moment: 'moment',
      }),
    ]
  },
  plugins: [
    '@/plugins/global-mixins.js',
    { src: '@/plugins/vuelidate.js', ssr: true },
    { src: '@/plugins/fb-sdk.js', ssr: false },
    { src: '@/plugins/use-jquery', ssr: false },
    { src: '@/plugins/datepicker.js', ssr: false },
    { src: '@/plugins/vue-notification.js', ssr: false },
    { src: '@/plugins/vue-lazyload.js', ssr: false },
    { src: '@/plugins/vue-cookie.js', ssr: false },
    { src: '@/plugins/click-outside.js', ssr: false },
    { src: '@/plugins/image-crop.js', ssr: false },
    { src: '@/plugins/ga.js', ssr: false },
    { src: '@/plugins/vue-mansonry.js', ssr: false }
    // { src: '@/plugins/Mansory.js', ssr: false }
  ],
  modules: [
    '@nuxtjs/font-awesome',
    '@nuxtjs/router',
    'cookie-universal-nuxt',
    '@nuxtjs/sitemap',
    // With options
    ['@nuxtjs/component-cache', {
      max: 10000,
      maxAge: 1000 * 60 * 60
    }],
    // With options
    ['nuxt-facebook-pixel-module', {
      /* module options */
      track: 'PageView',
      pixelId: '337898696702150',
    }],
  ],
  sitemap: {
    path: '/sitemap.xml',
    hostname: 'https://glamme.vn',
    cacheTime: 1000 * 60 * 15,
    generate: false, // Enable me when using nuxt generate
    exclude: [

    ],
    routes () {
      axios.defaults.baseURL = 'https://api.glamme.vn'
      return Promise.all(
        [
          axios.get('/cms/api/post?rowPerPage=500&status=1&type=0'),
          axios.get('/cms/api/category?rowPerPage=50'),
          axios.get('/cms/api/tag?rowPerPage=500')
        ]).then(values => {

        var allRouteXml = [];
        //landing page
        allRouteXml.push('/dich-vu')
        allRouteXml.push('/lam-dep-tan-noi')
        allRouteXml.push('/blog')
        allRouteXml.push('/kham-pha')
        //posts
        var posts = values[0].data.posts.map(p=> (
          {
            url: `/blog/${p.postSlug}.html`,
            changefreq: 'weekly',
            priority: 1,
            lastmodISO: p.createdDate
          }
        ))
        var categories = values[1].data.categories.map(c=>(
          {
            url: `/blog/${c.catSlug}`,
            changefreq: 'daily',
            priority: 1,
          }
        ))
        var tags = values[2].data.tags.map(t=>(
          {
            url: `/blog/tag/${t.tagSlug}.html`,
            changefreq: 'daily',
            priority: 1,
          }
        ))
        allRouteXml = allRouteXml.concat(categories)
        allRouteXml = allRouteXml.concat(posts)
        allRouteXml = allRouteXml.concat(tags)
        return allRouteXml
      })
    }
  },
  router: {
    middleware: [
      'inject-site-url',
      'fbsdk-parse',
      'preload-data',
      'check-auth',
      'redirect'
    ]
  },
  head: {
    htmlAttrs: {
      lang: 'vi-VN',
    },
    title: 'Glamme - Đặt lịch dịch vụ làm đẹp, trang điểm tận nơi',
    meta: [
      { charset: 'utf-8' },
      { name: 'language', content: 'vi-VN' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: "description", name: 'description', content: 'Gia nhập Glampy để cập nhật thông tin, xu hướng làm đẹp mới nhất và trải nghiệm những tiện ích làm đẹp dành riêng cho phụ nữ hiện đại' },
      { name: 'geo.region', content: 'VN' },
      { name: 'geo.placename', content: 'Ho Chi Minh City' },
      { name: 'geo.position', content: '10.76408;106.656341' },
      { name: 'ICBM', content: '10.76408, 106.656341' },
      //facebook open graph
      { property: "og:type", content: "article" },
      { hid: "og:title", property: "og:title", content: "Glamme - Đặt lịch dịch vụ làm đẹp, trang điểm tận nơi" },
      { hid: "og:url", property: "og:url", content: "https://glamme.vn" },
      { hid: "og:description", property: "og:description", content: "Gia nhập Glampy để cập nhật thông tin, xu hướng làm đẹp mới nhất và trải nghiệm những tiện ích làm đẹp dành riêng cho phụ nữ hiện đại" },
      { hid: "og:image", property: "og:image", content: "" },
      { property: "article:publisher", content: "https://www.facebook.com/Glampy.vn" },
      { property: "article:author", content: "https://www.facebook.com/Glampy.vn" },
      //dublink core
      { name: "DC.Title", content: "Glamme - Đặt lịch dịch vụ làm đẹp, trang điểm tận nơi" },
      { name: "DC.Creator", content: "Glamme Team" },
      { name: "DC.Subject", content: "Glamme" },
      { name: "DC.Description", content: "Glamme là dịch vụ đặt lịch làm đẹp tận nơi mang đến những gói dịch vụ trang điểm tận nơi từ chuyên viên trang điểm chuyên viên chuyên nghiệp ở bất cứ nơi, bất kỳ lúc nào các chị em muốn!" },
      { name: "DC.Publisher", content: "Glamme" },
      { name: "DC.Contributor", content: "Glamme Team" },
      { name: "DC.Date", content: "2018-03" },
      { name: "DC.Type", content: "text" },
      { name: "DC.Source", content: "Glamme.vn" },
      { name: "DC.Language", content: "vi" },
      { name: "DC.Coverage", content: "vietnam" },
      { name: "twitter:card", content: "summary_large_image" },
      //twitter card
      { name: "twitter:site", content: "@GlampyVN" },
      { name: "twitter:creator", content: "@GlampyVN" },
      { hid: "twitter:title", name: "twitter:title", content: "Chính sách sử dụng" },
      { hid: "twitter:description", name: "twitter:description", content: "" },
      { hid: "twitter:image", name: "twitter:image", content: "" },
      //facebook admin
      { property: "fb:pages", content: "1700708280179463" },
      { property: "fb:app_id", content: "742854255855497" },
      { property: "fb:admins", content: "100004488484051" },

    ],
    link: [
      { rel: "shortcut icon", type: 'image/x-icon', href: '/favicon.png' },
      { rel: "apple-touch-icon", type: "image/png", href: "/favicon.png" },
      { hid: "canonical", rel: "canonical", href: "https://glamme.vn" },
      { href:"https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700&subset=vietnamese", rel:"stylesheet"}
    ]
  },
  /*
  ** Global CSS
  */
  css: [
    '@/node_modules/bootstrap/dist/css/bootstrap.css',
    '@/node_modules/slick-carousel/slick/slick.css',
    '@/node_modules/slick-carousel/slick/slick-theme.scss',
    '@/node_modules/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css',
    '@/assets/css/components.scss',
    '@/assets/css/landing.scss',
    '@/assets/css/style.scss',
    '@/assets/css/mobile.scss',
  ],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  Custom route.js config for SPA
  Add this line when using SPA with @nuxtjs/router module
  */
  generate: {
    routes: [
      '/'
    ]
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend(config, { isDev, isClient, isServer }) {
      if (isServer) {
        config.externals = [
          require('webpack-node-externals')({
            whitelist: [/^vue-slick/]
          })
        ]
      }
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
