import locationApi from '@/assets/js/api/location'

const state = () => {
    locations: []
}

const actions = {
    async FETCH_LOCATIONS({commit}) {
        var rs = await locationApi.getProvince('');
        commit('SET_LOCATIONS',rs.data.data)
    }
}
const mutations = {
  SET_LOCATIONS(state,locations) {
    state.locations = locations
  }
}

const getters = {
  GET_LOCATIONS(state) {
    return state.locations
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}
