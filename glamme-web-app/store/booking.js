import artist from '@/assets/js/api/artist'

const state = () => ({
  listBookingHistoryPass: {},
  historyBookingType: 'uncomplete',
  isLoadMore: true
})

const actions = {

}
const mutations = {
  SET_LISTBOOKINGHISTORY(state, listBookingHistory) {
    state.listBookingHistoryPass = listBookingHistory
  },
  SET_DELETE_BOOKING(state, id){
    state.listBookingHistoryPass = state.listBookingHistoryPass.filter(s => s.booking.id !== id)
  },
  SET_BOOKING_HISTORY_LOADMORE(state, listBookingHistoryMore) {
    state.listBookingHistoryPass = state.listBookingHistoryPass.concat(listBookingHistoryMore)
  },
  SET_BOOKING_HISTORY_TYPE(state, statusType) {
    state.historyBookingType = statusType
  },
  SET_BOOKING_LOADMORE (state, status){
    state.isLoadMore = status
  }
}

const getters = {
  GET_LISTBOOKINGHISTORY (state){
    return state.listBookingHistoryPass
  },
  GET_BOOKING_HISTORY_TYPE(state) {
    return state.historyBookingType
  },
  GET_BOOKING_LOADMORE(state) {
    return state.isLoadMore
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}
