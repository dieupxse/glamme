import product from '@/assets/js/api/product'
import email from '@/assets/js/api/email'
const state =  {
  listProductTrangDiem: {}
}
const actions = {
  async nuxtServerInit ({ commit, dispatch}, context) {

  },
  async getProductTrangDiem({commit}, location, language='') {
    var dataParam = {
      cateId: 4,
      page: 1,
      orderby: 'id',
      order: 'desc',
      location: location,
      language: language
    }
    var rs = await product.getProductByCategoryId(
      dataParam.cateId,
      dataParam.page,
      dataParam.orderby,
      dataParam.order,
      dataParam.isshowhome,
      dataParam.location,
      dataParam.language
      );
    commit("SET_LIST_PRODUCT_TRANGDIEM", rs.data)
    return rs.data
  },
  getProductByCategorySlug ({commit}, dataParam) {
    return product.getProductByCategoryId(dataParam.cateId,
                                            dataParam.page,
                                            dataParam.orderby,
                                            dataParam.order,
                                            dataParam.isshowhome,
                                            dataParam.location,
                                            dataParam.language)
  },
  sendEmail ({commit}, email){
    return product.sendEmail(email)
  },
  getTemplateContentEmail ({commit}, dataParam){
    return email.getTemplateContentEmail(dataParam.groupId)
  }
}

const mutations = {
  SET_LIST_PRODUCT_TRANGDIEM(state, listProduct) {
    state.listProductTrangDiem = listProduct
  }
}

const getters = {
  GET_LISTPRODUCT_TRANGDIEM(state) {
    return state.listProductTrangDiem
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}
