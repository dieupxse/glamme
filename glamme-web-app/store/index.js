// jshint ignore: start
import Vuex from 'vuex'
import menu from './menu'
import post from './post'
import tag from './tag'
import banner from './banner'
import serviceCategory from './serviceCategory'
import artist from './artist'
import product from './product'
import discover from './discover'
import booking from './booking'
import location from './location'
import language from './language'

const createStore = () => {
  return new Vuex.Store({
    state: {
      headerMenus: {},
      footerDichvu: {},
      footerGlamme: {},
      showLoginModal: false,
      showNotifyModal: false,
      showLocationModal: false,
      authUser: null,
      seoHomePage: {},
      translate: {},
      currentLocation: 'ho-chi-minh',
      currentLang: 'vi',
      mobile: {
        isMobile: false,
        isTablet: false,
        isPhone: false,
        os: ''
      }
     },
    mutations: {
      SET_HEADER_MENU(state, headerMenus) {
        state.headerMenus = headerMenus
      },
      SET_DICHVU_MENU(state, footerDichvu) {
        state.footerDichvu = footerDichvu
      },
      SET_GLAMME_MENU(state, footerGlamme) {
        state.footerGlamme = footerGlamme
      },
      TOGGLE_LOGIN_MODAL(state, val) {
        state.showLoginModal = val
      },
      TOGGLE_NOTIFY_MODAL(state, val) {
        state.showNotifyModal = val
      },
      TOGGLE_LOCATION_MODAL(state, val) {
        state.showLocationModal = val
      },
      SET_AUTH(state, user) {
        state.authUser = user
      },
      REMOVE_AUTH(state) {
        state.authUser = null
        state.listBookingHistoryPass = null
      },
      SET_SEO_HOMEPAGE(state, seo) {
        state.seoHomePage = seo
      },
      SET_MOBILE(state,mobile) {
        state.mobile =mobile
      },
      SET_CURRENT_LOCATION(state, location) {
        state.currentLocation = location
      },
      SET_CURRENT_LANG(state, lang) {
        state.currentLang = lang
      },
      SET_TRANSLATE(state, trans) {
        state.translate = trans;
      }
    },
    actions: {
      /**
       * Các components cần SSR thì sẽ dispatch async action ở đây để get dữ liệu
       * dùng trong các component
       * Trong component dùng mapGetters để axtract dữ liệu :)
       *
       * @param {*} param0
       * @param {*} param1
       */
      async nuxtServerInit ({ commit, dispatch }, context) {
        var lang = context.params.lang
        await dispatch('FETCH_LOCATIONS');
        await dispatch('FETCH_LANGS');
        //get header menu for ssr

      }
    },
    getters: {
      GET_HEADER_MENU (state) {
        return state.headerMenus
      },
      GET_DICHVU_MENU (state) {
        return state.footerDichvu
      },
      GET_GLAMME_MENU (state) {
        return state.footerGlamme
      },
      IS_SHOW_LOGIN_MODAL (state) {
        return state.showLoginModal
      },
      IS_SHOW_NOTIFY_MODAL (state) {
        return state.showNotifyModal
      },
      IS_SHOW_LOCATION_MODAL (state) {
        return state.showLocationModal
      },

      GET_AUTH(state) {
        return state.authUser
      },
      GET_SEO_HOMEPAGE(state) {
        return state.seoHomePage
      },
      GET_MOBILE(state, mobile) {
        return state.mobile
      },
      GET_CURRENT_LOCATION(state) {
        return state.currentLocation
      },
      GET_CURRENT_LANG(state) {
        return state.currentLang
      },
      GET_TRANSLATE(state) {
        return state.translate;
      }
    },
    modules: {
     // category,
     menu,
     post,
     tag,
     // comments,
      banner,
      serviceCategory,
     // services,
      artist,
      product,
      discover,
      booking,
      location,
      language
    }
  })
}
export default createStore
