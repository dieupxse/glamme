import banner from '@/assets/js/api/banner'
const state =  {
  bannerItems: []
}
const actions = {
  getBannerItems ({commit}) {
    const bannerItems = banner.getBannerItems()
    commit('setBannerItemToState', bannerItems)
  }
}

const mutations = {
  setBannerItemToState (state, bannerItems) {
    state.bannerItems = bannerItems
  }
}

const getters = {
  allBannerItems (state) {
    return state.bannerItems
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}
