import artist from '@/assets/js/api/artist'

const state = () => {

}

const actions = {
  getListArtist({commit}, isHome){
    return artist.getListArtist(isHome)
  }
}
const mutations = {

}

const getters = {

}

export default {
  state,
  actions,
  mutations,
  getters
}
