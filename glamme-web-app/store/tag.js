// jshint ignore: start
import tag from '@/assets/js/api/tag'
const state = () =>  ({
  listMostUsedTags: []
})

const actions = {
  async getListMostUsedTags ({commit}, take) {
    var result = await tag.getMostUsedTag(take)
    commit('setListMostUsedTags', result)
  }
}

const mutations = {
  setListMostUsedTags (state, result) {
    state.listMostUsedTags = result
  }
}

const getters = {
  getMostUsedTag (state) {
    return state.listMostUsedTags
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}
