import langApi from '@/assets/js/api/language'

const state = () => {
    languages: []
}

const actions = {
    async FETCH_LANGS({commit}) {
        var rs = await langApi.getLanguage();
        commit('SET_LANGS',rs.data.data)
    }
}
const mutations = {
  SET_LANGS(state,languages) {
    state.languages = languages
  }
}

const getters = {
  GET_LANGS(state) {
    return state.languages
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}
