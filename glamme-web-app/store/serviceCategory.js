import serviceCategory from '@/assets/js/api/serviceCategory'
const state =  {

}
const actions = {
  getListServiceCategory ({commit}, dataParam) {
    return serviceCategory.getListServiceCategory(dataParam.artistId,
                                            dataParam.isShowHOme,
                                            dataParam.page,
                                            dataParam.orderby,
                                            dataParam.order)
  }
}

const mutations = {

}

const getters = {

}

export default {
  state,
  actions,
  mutations,
  getters
}
