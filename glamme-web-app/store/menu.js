import menu from '@/assets/js/api/menu'
const state = {
  blogMenuItems: [],
  homeMenuItems: [],
  headerMenuItems: [],
  menuItems: []
}
const actions = {
  getMenuItems ({commit}) {
    const menuItems = menu.getMenuItems()
    commit('setMenuItemToState', menuItems)
  },
  getBlogMenuItemsById ({commit}, id,lang) {
    return menu.getMenuItemsByMenuId(id,lang);
  }
}

const mutations = {
  setMenuItemToState (state, menuItems) {
    state.menuItems = menuItems
  },
  setBlogMenuItems (state, menuItems) {
    state.blogMenuItems = menuItems
  }
}

const getters = {
  allMenuItems (state) {
    return state.menuItems
  },
  getBlogMenuItems (state) {
    return state.blogMenuItems
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}
