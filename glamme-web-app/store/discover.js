const state =  {
  showDisCoverModal: false,
  ImageDiscover: {}
}
const actions = {
  SET_DISCOVER_MODAL_OPEN (context, data) {
    context.commit('OPEN_DISCOVER_MODAL', data)
  },
  SET_DISCOVER_MODAL_CLOSE (context) {
    context.commit('CLOSE_DISCOVER_MODAL')
  }
}

const mutations = {
  OPEN_DISCOVER_MODAL(state, data) {
    state.showDisCoverModal = true
    state.ImageDiscover = data
  },
  CLOSE_DISCOVER_MODAL(state){
    state.showDisCoverModal = false
  }
}

const getters = {
  DISCOVER_MODAL_STATUS: state => state.showDisCoverModal,
  DISCOVER_MODAL_IMAGES: state => state.ImageDiscover
}

export default {
  state,
  actions,
  mutations,
  getters
}
