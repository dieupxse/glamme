// jshint ignore: start
import posts from '@/assets/js/api/post'
import {POST_PER_PAGE} from '@/assets/js/commons/constants'
const state = () =>  ({
  loading: false,
  isLoadmore: true,
  post: {},
  categories: [],
  commentCount: 0,
  viewCount: 0,
  postsInHomePage: [],
  category: {},
  tag: {},
  listPosts: []
})

const actions = {
  getPost ({commit}) {
    var post = posts.getPost()
    commit('setPostToState', post)
  },
  getPostBySlug ({commit}, slug) {
    posts.getPostBySlug(slug).then((data) => {
      commit('setPostToState', data.post)
    }).catch((err)=> {
      console.log(err)
    })
  },
  updatePost ({commit}, post) {
    commit('setPostToState', post)
  },
  async getPostsByListCatId ({commit}, listCats) {
    var listPost = await posts.getPostsByListCatId(listCats)
    commit('setPostsByListCatIdToState', listPost)
  },
  async getListPorstsByCateSlug ({commit}, dataParam) {
    commit('setLoading', true)
    var data = await posts.getListPorstsByCateSlug(dataParam.slug, dataParam.status, dataParam.page);
    if (dataParam.page === 1) {
      commit('setListPostsToListPostState', data)
    } else {
      commit('setLoadMorePostToState', data)
    }
  },
  async getListPostsByTagSlug ({commit}, dataParam) {
    commit('setLoading', true)
    var data = await posts.getListPostsByTagSlug(dataParam.slug, dataParam.status, dataParam.page)
    if (dataParam.page === 1) {
      commit('setListPostsToListPostState', data)
    } else {
      commit('setLoadMorePostToState', data)
    }
  }
}

const mutations = {
  setPostToState (state, post) {
      state.post = post
  },
  setPostsByListCatIdToState (state, listPost) {
    state.postsInHomePage = listPost
  },
  setListPostsToListPostState (state, listPost) {
    state.listPosts = listPost.posts
    if(listPost.category) state.category = listPost.category
    if(listPost.tag) state.tag = listPost.tag
    state.isLoadmore = (state.listPosts.length >= POST_PER_PAGE)
    state.loading = false
  },
  setLoadMorePostToState (state, listPostLoadMore) {
    state.category = listPostLoadMore.category
    state.tag = listPostLoadMore.tag
    if (listPostLoadMore.posts.length) {
      state.listPosts = state.listPosts.concat(listPostLoadMore.posts)
      if (listPostLoadMore.posts.length < POST_PER_PAGE) state.isLoadmore = false
    } else {
      state.isLoadmore = false
    }
    state.loading = false
  },
  setLoading (state, val) {
    state.loading = val
  }
}

const getters = {
  getPostItem (state) {
    return state.post
  },
  getCurrentTag (state) {
    return state.tag
  },
  getPostsInHomepage (state) {
    return state.postsInHomePage
  },
  getListPostsToListPostState (state) {
    return state.listPosts
  },
  getCurrentCategory (state) {
    return state.category
  },
  isLoading (state) {
    return state.loading
  },
  isShowLoadmore (state) {
    return state.isLoadmore
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}
