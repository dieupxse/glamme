import Vue from 'vue'
import Router from 'vue-router'

import Index from '@/pages/index'
import Login from '@/pages/login'
import category from '@/pages/category/index'
//post page
import PostIndex from '@/pages/post/index'
import PostCreate from '@/pages/post/create'
//user page
import UserIndex from '@/pages/user/index'
// tag page
import TagIndex from '@/pages/tag/index'
//config page
import ConfigGroupText from '@/pages/config/grouptext'
import ConfigGroupTextDetail from '@/pages/config/grouptextdetail'
// SubscribeEmail
import SubscribeEmail from '@/pages/subscribeEmail/index'
// Management Product
import ArtistIndex from '@/pages/manage/artist/index'
import ArtistCreate from '@/pages/manage/artist/create'
import CategoryIndex from '@/pages/manage/category/index'
import ProductIndex from '@/pages/manage/product/index'
import ProductCreate from '@/pages/manage/product/create'
import ServiceIndex from '@/pages/manage/service/index'
import ImageIndex from '@/pages/manage/image/index'
import ImageCreate from '@/pages/manage/image/create'
import ProductTagIndex from '@/pages/manage/tag/index'
// menu
import MenuIndex from '@/pages/menu/index'
import MenuDetail from '@/pages/menu/detail'
// language
import LanguageIndex from '@/pages/language/index'
// province
import ProvinceIndex from '@/pages/province/index'
// booking
import BookingIndex from '@/pages/booking/index'
import BookingDetail from '@/pages/booking/detail'
// account
import AccountIndex from '@/pages/account/index'
// media
import MediaIndex from '@/pages/media/index'
// Discount code
import CampaignIndex from '@/pages/campaign/index'
import DiscountByCampaign from '@/pages/campaign/discountByCampaign'
import DiscountCodeDetail from '@/pages/campaign/discountCodeDetail'
import DiscountCodeIndex from '@/pages/campaign/discountCodeIndex'
import DiscountCodeCreate from '@/pages/campaign/createCode'
Vue.use(Router)

export function createRouter() {
  return new Router({
    mode: 'history',
    routes: [
      {
        name: 'Index',
        path: '/',
        component: Index
      },
      {
        name: 'UserIndex',
        path: '/user',
        component: UserIndex
      },
      {
        path: '/login',
        component: Login
      },
      {
        path: '/category',
        component: category
      },
      //post page
      {
        path: '/post',
        component: PostIndex
      },
      {
        path: '/post/create',
        component: PostCreate
      },
      {
        path: '/post/edit/:id',
        component: PostCreate
      },
      // tag page
      {
        path: '/tag',
        component: TagIndex
      },
      // config page
      {
        path: '/config/grouptext',
        component: ConfigGroupText
      },
      // config page
      {
        path: '/config/grouptext/:id',
        component: ConfigGroupTextDetail
      },
      // Subscribe email page
      {
        path: '/subscribe',
        component: SubscribeEmail
      },
      // Management page
      {
        path: '/manage/artist',
        component: ArtistIndex
      },
      {
        path: '/manage/artist/create',
        component: ArtistCreate
      },
      {
        path: '/manage/artist/edit/:id',
        component: ArtistCreate
      },
      // product category
      {
        path: '/manage/category',
        component: CategoryIndex
      },
      // product
      {
        path: '/manage/product',
        component: ProductIndex
      },
      {
        path: '/manage/product/create',
        component: ProductCreate
      },
      {
        path: '/manage/product/edit/:id',
        component: ProductCreate
      },
      // service
      {
        path: '/manage/service/:productId',
        component: ServiceIndex
      },
      // Image
      {
        path: '/manage/image',
        component: ImageIndex
      },
      {
        path: '/manage/image/create',
        component: ImageCreate
      },
      {
        path: '/manage/image/edit/:id',
        component: ImageCreate
      },
      // Tag product
      {
        path: '/manage/tag',
        component: ProductTagIndex
      },
      // menu
      {
        path: '/menu',
        component: MenuIndex
      },
      {
        path: '/menu/menudetail/:id',
        component: MenuDetail
      },
      // language
      {
        path: '/language',
        component: LanguageIndex
      },
      // province
      {
        path: '/province',
        component: ProvinceIndex
      },
      // booking
      {
        path: '/booking',
        component: BookingIndex
      },
      {
        path: '/booking/detail/:id',
        component: BookingDetail
      },
      // account
      {
        path: '/account',
        component: AccountIndex
      },
      // media
      {
        path: '/media',
        component: MediaIndex
      },
      // campaign
      {
        path: '/campaign',
        component: CampaignIndex
      },
      {
        path: '/discountdetail/:code',
        component: DiscountCodeDetail
      },
      {
        path: '/discount',
        component: DiscountCodeIndex
      },
      {
        path: '/discount/create',
        component: DiscountCodeCreate
      },
      {
        path: '/discount/:id',
        component: DiscountByCampaign
      },
    ],
    scrollBehavior: function (to, from, savedPosition) {
      return { x: 0, y: 0 }
    }
  })
}
