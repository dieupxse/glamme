const webpack = require('webpack')
module.exports = {
  build: {
    vendor: [
      'axios',
      'jquery',
      'vuelidate',
      'vue-notification',
      'vuejs-dialog',
      'vue-croppie',
      'moment'
    ],
    plugins: [
      
    ]
  },
  plugins: [
    '@/plugins/vuelidate.js',
    //{src: '@/plugins/config-editor.js', ssr:false},
    //{src: '@/plugins/tinymce-editor.js', ssr : false},
    //{src: '@/plugins/ck-editor.js', ssr : false},
    {src: '@/plugins/auth-cookies.js', ssr:false},
    {src: '@/plugins/image-crop.js', ssr:false},
    {src: '@/plugins/vue-confirm.js', ssr:false},
    {src: '@/plugins/theme.js', ssr:false},
    {src: '@/plugins/use-jquery', ssr: false},
    {src: '@/assets/js/adminlte.js', ssr: false},
    {src: '@/node_modules/bootstrap/dist/js/bootstrap.js', ssr: false},
    {src: '@/plugins/vue-notification.js', ssr: false},
    {src: '@/plugins/datetimepicker.js', ssr: false},
    {src: '@/plugins/click-outside.js', ssr: false },
    {src: '@/plugins/vue-mansory.js', ssr: false },
  ],
  modules: [
    '@nuxtjs/font-awesome',
    '@nuxtjs/router',
    'cookie-universal-nuxt',
  ],
  router: {
    middleware: [
      'check-auth'
    ]
  },
  head: {
    htmlAttrs: {
      lang: 'vi-VN',
    },
    bodyAttrs: {
      class: 'hold-transition skin-blue sidebar-mini'
    },
    title: 'Glamme Admin CMS',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Glamme CMS' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic'}
    ],
    script: [
      {src: '//cdn.ckeditor.com/4.8.0/full/ckeditor.js'},
      //{src: '/js/adminlte.min.js'}
    ]
  },
  /*
  ** Global CSS
  */
  css: [
   '@/node_modules/bootstrap/dist/css/bootstrap.css',
   '@/assets/css/AdminLTE.css',
   '@/assets/css/skins/skin-blue.css',
   '@/assets/css/style.scss'
  ],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#fff' },
  /*
  Custom route.js config for SPA
  Add this line when using SPA with @nuxtjs/router module
  */
  generate: {
    routes: [
      '/'
    ]
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
