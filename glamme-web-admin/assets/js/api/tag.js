import {http} from './helper'
export default {
  getListTags (token, keyword='', page=1, rowPerPage=10, orderby='CreatedDate', order="desc") {
    return new Promise((resolve, reject) => {
      http(token).get(`/cms/api/tag`, {
        params: {
          page: page,
          rowPerPage: rowPerPage,
          keyword: keyword,
          orderby: orderby,
          order: order,
        }
      }).then(response => {
        resolve(response)
      }).catch(err => {
        reject(err)
      })
    })
  },
  createTag(token, tag) {
    return new Promise((resolve, reject) => {
      http(token).post(`/cms/api/tag`, tag).then(response => {
        resolve(response)
      }).catch(err => {
        reject(err)
      })
    })
  },
  updateTag(token, tagId ,tag){
    return new Promise((resolve, reject) => {
      http(token).put(`/cms/api/tag/${tagId}`, tag).then(response => {
        resolve(response)
      }).catch(err => {
        reject(err)
      })
    })
  },
  getTagByTagId(token, tagId){
    return new Promise((resolve, reject) => {
      http(token).get(`/cms/api/tag/${tagId}`).then(response => {
        resolve(response.data)
      }).catch(err => {
        reject(err)
      })
    })
  },
  deleteListTag (token, listTagIds) {
    return new Promise((resolve, reject) => {
      http(token).delete(`cms/api/tag/DeleteList`, {
        params: {
          listIds: listTagIds.join(',')
        }
      }).then(response => {
        resolve(response)
      }).catch(err => {
        reject(err)
      })
    })
  },
}