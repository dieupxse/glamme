import {http} from './helper'
export default {
  getProductCategories(token, query) {
    return new Promise((resolve, reject) => {
      http(token).get(`/goods/api/category`, {
        params: query
      }).then(response => {
        resolve(response.data)
      }).catch(err => {
        reject(err)
      })
    })
  },
  getCategoryById(token, id){
    return new Promise((resolve, reject) => {
      http(token).get(`/goods/api/category/${id}`).then(response => {
        resolve(response.data)
      }).catch(err => {
        reject(err)
      })
    })
  },
  createCategory (token, category) {
    return new Promise((resolve, reject)=>{
      http(token).post(`/goods/api/category`, category).then(response => {
        resolve(response)
      }).catch(err => {
        reject(err)
      })
    })
  },
  updateCategory (token, id, category) {
    return new Promise((resolve, reject) => {
      http(token).put(`/goods/api/category/${id}`, category).then(response => {
        resolve(response)
      }).catch(err => {
        reject(err)
      })
    })
  },
  deleteCategory (token, id) {
    return new Promise((resolve, reject) => {
      http(token).delete(`/goods/api/category/${id}`).then(response => {
        resolve(response)
      }).catch(err => {
        reject(err)
      })
    })
  }
}
