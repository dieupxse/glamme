import {http} from './helper'
export default {
  getListPosts (token, catId, language, type, status, page, rowPerPage, keyword, orderby, order) {

    return new Promise((resolve, reject) => {
      http(token).get(`/cms/api/post/GetListPostForCMS`, {
        params: {
          cat: catId,
          language:language,
          type: type,
          status: status,
          page: page,
          rowPerPage: rowPerPage,
          keyword: keyword,
          orderby: orderby,
          order: order,
        }
      }).then(response => {
        resolve(response)
      }).catch(err => {
        reject(err)
      })
    })
  },
  deleteListPost(token, listPostIds) {
    console.log(listPostIds)
    return new Promise((resolve, reject) => {
      http(token).delete(`cms/api/post/DeleteList`, {
        params: {
          listIds: listPostIds.join(',')
        }
      }).then(response => {
        console.log(response)
        resolve(response)
      }).catch(err => {
        reject(err)
      })
    })
  },
  getPostById(token, postId) {
    return new Promise((resolve, reject) => {
      http(token).get(`/cms/api/post/${postId}`).then(response => {
        resolve(response.data)
      }).catch(err => {
        reject(err)
      })
    })
  },
  createPost(token, postData) {
    return new Promise((resolve, reject) => {
      http(token).post(`/cms/api/post`, postData).then(response => {
        resolve(response)
      }).catch(err => {
        reject(err)
      })
    })
  },
  updatePost(token, postId, postData) {
    return new Promise((resolve, reject) => {
      http(token).put(`/cms/api/post/${postId}`, postData).then(response => {
        resolve(response)
      }).catch(err => {
        reject(err)
      })
    })
  }
}
