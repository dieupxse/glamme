import {http} from './helper'
export default {
  getTags (token, params){
    return new Promise((resolve, reject) => {
      http(token).get(`/goods/api/tag/`, { params: params}).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  getTagById (token, id){
    return new Promise((resolve, reject) => {
      http(token).get(`/goods/api/tag/GetTagById/${id}`).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  createTag(token,  data){
    return new Promise((resolve, reject) => {
      http(token).post(`/goods/api/tag/`,data).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  updateTag(token, id, data){
    return new Promise((resolve, reject) => {
      http(token).put(`/goods/api/tag/${id}`,data).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  deleteTag(token, id){
    return new Promise((resolve, reject) => {
      http(token).delete(`/goods/api/tag/${id}`).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  }
}