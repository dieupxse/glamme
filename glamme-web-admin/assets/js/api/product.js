import {http} from './helper'
export default {
  getListProducts (token, params) {
    return new Promise((resolve, reject) => {
      http(token).get(`/goods/api/Product`, {
        params: params
      }).then (response => {
        resolve(response.data)
      }).catch(err => {
        reject(err)
      })
    })
  },
  getProductById (token, id) {
    return new Promise((resolve, reject) => {
      http(token).get(`/goods/api/Product/${id}`).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err)
      })
    })
  },
  createProduct (token, data) {
    return new Promise((resolve, reject) => {
      http(token).post(`goods/api/Product`, data).then(rs => {
        resolve(rs)
      }).catch(err => {
        reject(err)
      })
    })
  },
  updateProduct (token, id, data) {
    return new Promise((resolve, reject) => {
      http(token).put(`goods/api/Product/${id}`, data).then(rs => {
        resolve(rs)
      }).catch(err => {
        reject(err)
      })
    })
  },
  deleteProduct(token, id){
    return new Promise((resolve, reject) => {
      http(token).delete(`goods/api/Product/${id}`).then(rs => {
        resolve(rs)
      }).catch(err => {
        reject(err)
      })
    })
  }
}
