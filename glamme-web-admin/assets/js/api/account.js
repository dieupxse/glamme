import {http} from './helper'
export default {
  getListAccounts (token, params) {
    return new Promise((resolve, reject) => {
      http(token).get(`/account/api/account`, {params : params}).then(response => {
        resolve(response.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  getAccountById (token, id){
    return new Promise((resolve, reject) => {
      http(token).get(`/account/api/account/GetAccountById/${id}`).then(response => {
        resolve(response.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  }
}
