import {http} from './helper'
// import axios from 'axios'

// const http1 = (token) =>  axios.create({
//   headers: {
//     Authorization: `Bearer ${token}`,
//     "content-type": "application/json",
//   },
//   baseURL: '',
// })
export default {
  getLanguage(token, keyword="", page=1, rowPerPage=20, orderby="CreateDate", order="desc") {
    return new Promise((resolve, reject) => {
        http(token).get(`/config/api/language`, {
            params: {
                keyword,
                page,
                rowPerPage,
                orderby,
                order
            }
        }).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
    })
  },
  getLanguageBySlug(token, slug) {
    return new Promise((resolve, reject) => {
        http(token).get(`/config/api/language/slug/${slug}`).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
    })
  },
  getLanguageById(token, id) {
    return new Promise((resolve, reject) => {
        http(token).get(`/config/api/language/${id}`).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
    })
  },
  createLanguage(token, data) {
    return new Promise((resolve, reject) => {
        http(token).post(`/config/api/language`, data).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
      })
  },
  updateLanguage(token,id,data) {
    return new Promise((resolve, reject) => {
        http(token).put(`/config/api/language/${id}`, data).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
      })
  },
  deleteLanguage(token,id) {
    return new Promise((resolve, reject) => {
        http(token).delete(`/config/api/language/${id}`).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
      })
  }
}
