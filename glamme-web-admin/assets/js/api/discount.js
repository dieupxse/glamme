import {http} from './helper'
export default {
  getListCampaigns (token, params) {
    return new Promise((resolve, reject) => {
      http(token).get(`/code/api/Campaign`, {params: params}).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  getCampaignById (token, id) {
    return new Promise((resolve, reject) => {
      http(token).get(`/code/api/Campaign/${id}`).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  createCampaign (token, data) {
    return new Promise((resolve, reject) => {
      http(token).post(`/code/api/Campaign`, data).then(rs => {
        resolve(rs)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  updateCampaign (token, data) {
    return new Promise((resolve, reject) => {
      http(token).put(`/code/api/Campaign`, data).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  deleteCampaign (token, data) {
    return new Promise((resolve, reject) => {
      http(token).delete(`/code/api/Campaign`, {data: data}).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  getListCodeByCampId (token, params) {
    return new Promise((resolve, reject) => {
      http(token).get(`/code/api/DiscountCode`, {params: params}).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  getCodeByCode (token, code) {
    return new Promise((resolve, reject) => {
      http(token).get(`/code/api/DiscountCode/codeParam/${code}`).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  createCode (token, data) {
    return new Promise((resolve, reject) => {
      http(token).post(`/code/api/DiscountCode`, data).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  activeCode (token, id) {
    return new Promise((resolve, reject) => {
      http(token).put(`/code/api/DiscountCode/activecode/${id}`).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  deleteCode (token, data) {
    return new Promise((resolve, reject) => {
      http(token).delete(`/code/api/DiscountCode`, {data: data}).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  getDetailCode (token, code, params) {
    return new Promise((resolve, reject) => {
      http(token).get(`/code/api/DiscountCode/codeParam/${code}`, {params: params}).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  }
}