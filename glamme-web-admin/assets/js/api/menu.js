import {http} from './helper'
export default {
  getMenus (token) {
    return new Promise((resolve, reject) => {
      http(token).get(`/config/api/menu`).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err)
      })
    })
  },
  getMenuById (token, id) {
    var params = {
      orderBy: 'order',
      order: 'asc'
    }
    return new Promise((resolve, reject) => {
      http(token).get(`/config/api/menu/${id}?isAdmin=true`, {params: params}).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err)
      })
    })
  },
  createMenu (token, data) {
    return new Promise((resolve, reject) => {
      http(token).post(`/config/api/menu/`, data).then(rs => {
        resolve(rs)
      }).catch(err => {
        reject(err)
      })
    })
  },
  updateMenu (token, id, data) {
    return new Promise((resolve, reject) => {
      http(token).put(`/config/api/menu/${id}`, data).then(rs => {
        resolve(rs)
      }).catch(err => {
        reject(err)
      })
    })
  },
  deleteMenu (token, id){
    return new Promise((resolve, reject) => {
      http(token).delete(`/config/api/menu/${id}`).then(rs => {
        resolve(rs)
      }).catch(err => {
        reject(err)
      })
    })
  },
  // Menu Detail
  getMenuDetailById(token, id){
    return new Promise((resolve, reject) => {
      http(token).get(`/config/api/MenuDetail/${id}?isAdmin=true`).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err)
      })
    })
  },
  createMenuDetail (token, data){
    return new Promise((resolve, reject) => {
      http(token).post(`/config/api/MenuDetail`, data).then(rs => {
        resolve(rs)
      }).catch(err => {
        reject(err)
      })
    })
  },
  updateMenuDetail (token, id, data){
    if(data.order){
      data.order = parseInt(data.order)
    }
    return new Promise((resolve, reject) => {
      http(token).put(`/config/api/MenuDetail/${id}`, data).then(rs => {
        resolve(rs)
      }).catch(err => {
        reject(err)
      })
    })
  },
  deleteMenuDetail(token, id){
    return new Promise((resolve, reject) => {
      http(token).delete(`/config/api/MenuDetail/${id}`).then(rs => {
        resolve(rs)
      }).catch(err => {
        reject(err)
      })
    })
  }
}
