import {axios, http} from './helper'

export default {
  getAllCategories (page=1, rowPerPage=20, orderby='createdDate', order="desc") {
    return new Promise((resolve, reject) => {
      axios.get(`/cms/api/category?page=${page}&rowPerPage=${rowPerPage}&keyword=&orderby=${orderby}&order=${order}`)
        .then(response => {
          resolve(response.data)
        }).catch(err => {
          reject(err)
        })
    })
  },
  getCategoryDetail (id) {
    return new Promise((resolve, reject) => {
      axios.get(`/cms/api/category/${id}`)
        .then(response => {
          resolve(response.data)
        }).catch(err => {
          reject(err)
        })
    })
  },
  editCategory (token, detail) {
    return new Promise((resolve, reject) => {
      http(token).put(`/cms/api/category/${detail.id}`, detail)
      .then(response => {
      resolve(response)
      }).catch(err => {
      reject(err)
      })
    })
  },
  addNewCategory (token, detail) {
    return new Promise((resolve, reject) => {
      http(token).post(`/cms/api/category`, detail)
      .then(response => {
      resolve(response)
      }).catch(err => {
      reject(err)
      })
    })
  },
  deleteCategory (token, id) {
    return new Promise((resolve, reject) => {
      http(token).delete(`/cms/api/category/${id}`)
      .then(response => {
      resolve(response)
      }).catch(err => {
      reject(err)
      })
    })
  }
}
