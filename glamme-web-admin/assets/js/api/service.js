import {http} from './helper'

export default {
  getServices (token, params) {
    return new Promise((resolve, reject) => {
      http(token).get(`/goods/api/Service`, { params: params}).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err)
      })
    })
  },
  getServiceById (token, id) {
    return new Promise((resolve, reject) => {
      http(token).get(`/goods/api/Service/${id}`).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err)
      })
    })
  },
  getServiceByProductId (token, productId) {
    return new Promise((resolve, reject) => {
      http(token).get(`/goods/api/Service/getbyproduct/${productId}`).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err)
      })
    })
  },
  createService (token, data) {
    return new Promise((resolve, reject) => {
      http(token).post(`goods/api/Service`, data).then(rs => {
        resolve(rs)
      }).catch(err => {
        reject(err)
      })
    })
  },
  updateService (token, id, data) {
    return new Promise((resolve, reject) => {
      http(token).put(`goods/api/Service/${id}`, data).then(rs => {
        resolve(rs)
      }).catch(err => {
        reject(err)
      })
    })
  },
  deleteService (token, id) {
    return new Promise((resolve, reject) => {
      http(token).delete(`/goods/api/Service/${id}`).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err)
      })
    })
  }
}