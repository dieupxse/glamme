import {http} from './helper'
export default {
  getListSubscribeEmail (token, params) {
    return new Promise((resolve, reject) => {
      http(token).get(`/user/api/Subscrible`, {
        params : params
      })
      .then(response => {
        resolve(response.data)
      }).catch(err => {
        reject(err)
      })
    })
  }
}