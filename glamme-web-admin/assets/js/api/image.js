import {http} from './helper'
export default {
  getImages (token, params) {
    return new Promise((resolve, reject) => {
      http(token).get('/goods/api/image/', {params: params}).then( response => {
        resolve(response)
      }).catch(error => {
        reject(error.response)
      })
    })
  },
  getImageById(token, id){
    return new Promise((resolve, reject) => {
      http(token).get(`/goods/api/image/GetImageById/${id}`).then( response => {
        resolve(response.data)
      }).catch(error => {
        reject(error.response)
      })
    })
  },
  createImage(token, data){
    return new Promise((resolve, reject) => {
      http(token).post(`/goods/api/image/`, data).then( response => {
        resolve(response)
      }).catch(error => {
        reject(error.response)
      })
    })
  },
  updateImage(token, id, data){
    return new Promise((resolve, reject) => {
      http(token).put(`/goods/api/image/${id}`, data).then( response => {
        resolve(response)
      }).catch(error => {
        reject(error.response)
      })
    })
  },
  deleteImage (token, id) {
    return new Promise((resolve, reject) => {
      http(token).delete(`/goods/api/image/${id}`,).then( response => {
        resolve(response)
      }).catch(error => {
        reject(error.response)
      })
    })
  }
}
