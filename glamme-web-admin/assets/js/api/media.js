import {http} from './helper'
export default {
  uploadPhoto (token, info) {
    return new Promise((resolve, reject) => {
      http(token).post(`/media/api/photo`, info).then(response => {
        resolve(response.data)
      }).catch(err => {
        reject(err)
      })
    })
  },
  getListMedias (token, params) {
    return new Promise((resolve, reject) => {
      http(token).get(`/media/api/photo`, {params: params}).then(response => {
        resolve(response.data)
      }).catch(error => {
        reject(error)
      })
    }) 
  }
}