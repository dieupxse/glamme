import {http} from './helper'
export default {
  createArtist(token, data) {
    return new Promise((resolve, reject) => {
      http(token).post(`/goods/api/Artist`, data).then(response => {
        resolve(response)
      }).catch(err => {
        reject(err)
      })
    })
  },
  updateArtist(token, id, data) {
    return new Promise((resolve, reject) => {
      http(token).put(`/goods/api/Artist/${id}`, data).then(response => {
        resolve(response)
      }).catch(err => {
        reject(err)
      })
    })
  },
  getArtists(token, query) {
    return new Promise((resolve, reject) => {
      http(token).get(`/goods/api/Artist`, {
        params: query
      }).then(response => {
        resolve(response)
      }).catch(err => {
        reject(err)
      })
    })
  },
  getArtistById(token, id) {
    return new Promise((resolve, reject) => {
      http(token).get(`/goods/api/Artist/${id}`).then(response => {
        resolve(response)
      }).catch(err => {
        reject(err)
      })
    })
  },
  deleteArtist(token, id) {
    return new Promise((resolve, reject) => {
        http(token).delete(`/goods/api/Artist/${id}`).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
      })
  },
  getAllProvince () {
    return new Promise((resolve, reject) => {
      http('').get(`ultil/api/ulitity/GetAllProvince`)
        .then(response => {
          resolve(response.data)
        }).catch(err => {
          reject(err)
        })
    })
  },
  getDistrictsByProvinceId (id) {
    return new Promise((resolve, reject) => {
      http('').get(`ultil/api/ulitity/GetDistrictsByProvinceId/${id}`)
        .then(response => {
          resolve(response.data)
        }).catch(err => {
          reject(err)
        })
    })
  }
}
