import {http} from './helper'

export default {
  getListBooking(token, params) {
    return new Promise((resolve, reject) => {
      http(token).get(`/booking/api/booking/`, {params : params}).then(response => {
        resolve(response.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  getDetailBooking (token, id) {
    return new Promise((resolve, reject) => {
      http(token).get(`/booking/api/booking/GetBookingByIdCRM/${id}`).then(response => {
        resolve(response.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  updateBookingStatus (token, id, status) {
    return new Promise((resolve, reject) => {
      http(token).put(`/booking/api/booking/${id}?status=${status}` ).then(response => {
        resolve(response.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  }
}