const API_BASE_URL = 'https://api.glamme.vn'
const POST_PER_PAGE = 10
const POST_PER_PAGE_IN_SERVICE_CATEGORY = 9
const RECENT_VIEW_PRODUCT_NUMBER = 5
const LOCAL_STORAGE_CART_KEY = 'cart'
const LOCAL_STORAGE_RECENT_VIEW_PRODUCT_KEY = 'recent_view_product'
const SITE_URL = 'https://glamme.vn'
const postStatus = [
  {
    name: 'Bản nháp (Draf)',
    value: 0
  },
  {
    name: 'Đã đăng (Publish)',
    value: 1
  },
  {
    name: 'Đã ẩn (Hidden)',
    value: 2
  }
]
const postType = [
  {
    name: 'Bài viết (Post)',
    value: 0
  },
  {
    name: 'Trang nội dung (Page)',
    value: 1
  },
  {
    name: 'Bài viết Video (Video)',
    value: 2
  },
  {
    name: 'Bài viết Gallery (Gallery)',
    value: 3
  }
]
const callApi = function () {
    alert('call api')
}
const apiUrl = {
    MENU_DETAIL: `${API_BASE_URL}/config/api/MenuDetail`,
    MENU: `${API_BASE_URL}/config/api/menu/`
}
const gaConfig = {
    id: 'UA-XXXXXXXX-X'
}
const fbSdkConfig = {
    appId: '1904896796489976',
    cookie: true,
    xfbml: true,
    autoLogAppEvents : true,
    version: 'v2.12'
  }
const AUTH_TOKEN_KEY = '_ZKV'
const MENU_OBJECT_TYPE = [
  {
    name: 'custom-link',
    value: 'custom-link'
  },
  {
    name: 'category',
    value: 'category'
  },
  {
    name: 'page',
    value: 'page'
  }
]
const BOOKING_STATUS = [
  {
    name: 'Đang chờ xử lý',
    value: 1
  },
  {
    name: 'Đã xác nhận',
    value: 2
  },
  {
    name: 'Đã hoàn thành',
    value: 3
  },
  {
    name: 'Hủy',
    value: 4
  }
]

export {
    SITE_URL,
    API_BASE_URL,
    POST_PER_PAGE,
    POST_PER_PAGE_IN_SERVICE_CATEGORY,
    RECENT_VIEW_PRODUCT_NUMBER,
    LOCAL_STORAGE_CART_KEY,
    LOCAL_STORAGE_RECENT_VIEW_PRODUCT_KEY,
    callApi,
    postStatus,
    postType,
    apiUrl,
    fbSdkConfig,
    gaConfig,
    AUTH_TOKEN_KEY,
    MENU_OBJECT_TYPE,
    BOOKING_STATUS
}

