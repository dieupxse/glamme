import tag from '@/assets/js/api/tag'

const state = () => ({
  listTags: [],
  params: {
    page: 1,
    rowPerPage: 30,
    keyword: '',
    orderby: 'CreatedDate',
    order: 'desc'
  }
})

const actions = {
  actionGetListTags ({commit}, params) {
    const {token, keyword, page, rowPerPage, orderby, order} = params
    commit('setSearchParams', params)
    tag.getListTags(token, keyword, page, rowPerPage, orderby, order).then((rs)=> {
      commit('setListTags',rs.data)
    })
  }
}

const mutations = {
  setListTags (state, data) {
    state.listTags = data
  },
  setSearchParams (state, params) {
    state.params = params
  }
}

const getters = {
  getListTags (state) {
    return state.listTags
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}  