import blogCategory from '@/assets/js/api/BlogCategory'
// import {POST_PER_PAGE} from '@/assets/js/commons/constants'
const state = () =>  ({
  listBlogCategory: []
})
const actions = {
  getAllCategories ({commit}, dataParam) {
    return blogCategory.getAllCategories(dataParam.page,
                                      dataParam.rowPerPage,
                                      dataParam.orderby,
                                      dataParam.order)
  },
  getCategoryDetail ({commit}, id) {
    return blogCategory.getCategoryDetail(id)
  },
  editCategory ({commit}, dataParam) {
    return blogCategory.editCategory(dataParam.token, dataParam.detail)
  },
  addNewCategory ({commit}, dataParam) {
    return blogCategory.addNewCategory(dataParam.token, dataParam.detail)
  },
  deleteCategory ({commit}, dataParam) {
    return blogCategory.deleteCategory(dataParam.token, dataParam.id)
  }
}

const mutations = {
  setListBlogCategory (state, listBlogCategory) {
    state.listBlogCategory = listBlogCategory
  }
}

const getters = {
  getlistBlogCategory (state) {
    return state.listBlogCategory
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}
