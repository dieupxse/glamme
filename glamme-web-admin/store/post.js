import post from '@/assets/js/api/post'

const state = () => ({
  listPosts: [],
  params: {
    cat: 0,
    language:'',
    type: -1,
    status: -1,
    page: 1,
    rowPerPage: 20,
    keyword: '',
    orderby: 'CreatedDate',
    order: 'desc'
  },
  showMediaModal: false,
  functionClickImageModal: ''
})

const actions = {
  actionGetListPosts ({commit}, params) {
    const {token, catId, language, type, status, page, rowPerPage, keyword, orderby, order} = params
    commit('setSearchParams', params)
    post.getListPosts(token, catId, language, type, status, page, rowPerPage, keyword, orderby, order).then((rs)=> {
      commit('setListPosts',rs.data)
    })
  },
  SET_MEDIA_MODAL_OPEN (context,val) {
    context.commit('OPEN_MEDIA_MODAL',val)
  },
  SET_MEDIA_MODAL_CLOSE (context) {
    context.commit('CLOSE_MEDIA_MODAL')
  }
}

const mutations = {
  setListPosts (state, data) {
    state.listPosts = data
  },
  setSearchParams (state, params) {
    state.params = params
  },
  OPEN_MEDIA_MODAL(state,val) {
    state.showMediaModal = true
    state.functionClickImageModal = val
  },
  CLOSE_MEDIA_MODAL(state){
    state.showMediaModal = false
  }
}

const getters = {
  getListPosts (state) {
    return state.listPosts
  },
  MEDIA_MODAL_STATUS: state => state.showMediaModal,
  MEDIA_MODAL_FUNCTION: state => state.functionClickImageModal
}

export default {
  state,
  actions,
  mutations,
  getters
}
