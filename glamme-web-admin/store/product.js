import productApi from '@/assets/js/api/product'

const state = () => ({
  listProducts: [],
  params: {
    categoryId: 0,
    isShowHome: -1,
    isShowWidget: -1,
    page: 1,
    rowPerPage: 10,
    keyword: '',
    orderby: 'CreateDate',
    order: 'desc'
  }
})

const actions = {
  actionGetListProducts ({commit}, params) {
    commit('setSearchParams', params.searchParams)
    productApi.getListProducts(params.token, params.searchParams).then((rs)=> {
      commit('setListProducts',rs)
    })
  }
}

const mutations = {
  setListProducts (state, data) {
    state.listProducts = data
  },
  setSearchParams (state, params) {
    state.params = params
  }
}

const getters = {
  getListProducts (state) {
    return state.listProducts
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}  