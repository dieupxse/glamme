# glamme-web-admin

> Nuxt.js project

## Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```
## Docker guilde
``` bash
## GLAMME BUILD GUILD
# build for admin

$ docker build -t glamme/admin  .
$ docker run -d -p 3001:3001 --name glamme_admin glamme/admin
# if docker exist then delete
$ docker rm <containerid>

# access container
$ docker exec -it <container name> /bin/bash
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).
